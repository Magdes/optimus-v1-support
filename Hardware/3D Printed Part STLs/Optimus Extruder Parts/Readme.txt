Extruder Plastic Part STL - Provided by Bastian M�ller

Open and slice it in Cura.
No support needed.
After printing, you will need to drill the grounded filament hole
with a 3mm bit.
A big thank you to Bastian M�ller for sharing his model with us. 