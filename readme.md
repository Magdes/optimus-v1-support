# Overview
The Febtop Optimus V1 is a 3-in-1 CNC/Laser/3D Printer originally launched via Indiegogo in 2017 https://www.indiegogo.com/projects/optimus-a-transformable-3d-printer#/  

The company has moved on to more recent models (https://www.optimus3dp.com/) so I've created this space to track my tips and tricks and upgrades.  

The Optimus uses a Smoothieboard V1 (https://smoothieware.org/) so some of these updates may be useful to anyone using a similar controller.  

# Wiki
My operating procedures can be found here: [Optimus V1 Wiki](https://gitlab.com/Magdes/optimus-v1-support/-/wikis/home)

# Cloneing
Note that this repo contains submodules so your best bet while cloning is:
git clone --recurse-submodules git@gitlab.com:<repo url>